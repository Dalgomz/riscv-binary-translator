class Data:
	def __init__(self, name:str, value:int=None):
		if name[0:4] == 'imm-':
			self.fixed = False
			if name[-3:] == 'msb' or name[-3:] == 'lsb':
				self.size = 12
			else:
				self.size = int(name[4:])
		elif name[0] == 'b':
			self.fixed = True
			self.size = len(name)-1
			value = int(name[1:], base=2)
		else:
			self.fixed = False
			self.size = 5

		if value is not None:
			self.value = value
			tempBin = bin(value)
			missingDigits = self.size - (len(tempBin) - 2)
			missingDigits = ''.join(['0' for i in range(missingDigits)])
			self.binValue = missingDigits + tempBin[2:]
		else:
			self.binValue = ''.join(['0' for i in range(self.size)])

instructionMappings = {
	'lui': ['imm-20','rd','b01101','b11'],
	'auipc': ['imm-20','rd','b00101','b11'],
	'addi':	['imm-12','rs1','b000','rd','b00100','b11'],
	'slti':	['imm-12','rs1','b010','rd','b00100','b11'],
	'sltiu': ['imm-12','rs1','b011','rd','b00100','b11'],
	'xori': ['imm-12','rs1','b100','rd','b00100','b11'],
	'ori': ['imm-12','rs1','b110','rd','b00100','b11'],
	'andi': ['imm-12','rs1','b111','rd','b00100','b11'],
	'slli': ['b0000000','shamt','rs1','b001','rd','b00100','b11'],
	'srli': ['b0000000','shamt','rs1','b101','rd','b00100','b11'],
	'srai': ['b0100000','shamt','rs1','b101','rd','b00100','b11'],
	'add': ['b0000000','rs2','rs1','b000','rd','b01100','b11'],
	'sub': ['b0100000','rs2','rs1','b000','rd','b01100','b11'],
	'sll': ['b0000000','rs2','rs1','b001','rd','b01100','b11'],
	'slt': ['b0000000','rs2','rs1','b010','rd','b01100','b11'],
	'sltu': ['b0000000','rs2','rs1','b011','rd','b01100','b11'],
	'xor': ['b0000000','rs2','rs1','b100','rd','b01100','b11'],
	'srl': ['b0000000','rs2','rs1','b101','rd','b01100','b11'],
	'sra': ['b0100000','rs2','rs1','b101','rd','b01100','b11'],
	'or': ['b0000000','rs2','rs1','b110','rd','b01100','b11'],
	'and': ['b0000000','rs2','rs1','b111','rd','b01100','b11'],
	'fence': ['b0000','pred','succ','00000','000','b00000','b00011','b11'],
	'fence.i': ['b0000','b0000','b0000','b00000','b001','b00000','b00011','b11'],
	'csrrw': ['imm-12','rs1','b001','rd','b11100','b11'],
	'csrrs': ['imm-12','rs1','b010','rd','b11100','b11'],
	'csrrc': ['imm-12','rs1','b011','rd','b11100','b11'],
	'csrrwi': ['imm-12','Data','b101','rd','b11100','b11'],
	'csrrsi': ['imm-12','Data','b110','rd','b11100','b11'],
	'csrrci': ['imm-12','Data','b111','rd','b11100','b11'],
	'ecall': ['b0000000','b00000','b00000','b000','b00000','b11100','b11'],
	'ebreak': ['b0000000','b00001','b00000','b000','b00000','b11100','b11'],
	'uret': ['b0000000','b00010','b00000','b000','b00000','b11100','b11'],
	'sret': ['b0001000','b00010','b00000','b000','b00000','b11100','b11'],
	'mret': ['b0011000','b00010','b00000','b000','b00000','b11100','b11'],
	'wfi': ['b0001000','b00101','b00000','b000','b00000','b11100','b11'],
	'sfence.vma': ['b0001001','rs2','rs1','b000','rd','b11100','b11'],
	'lb': ['imm-12','rs1', 'b000','rd','b00000','b11'],
	'lh': ['imm-12','rs1', 'b001','rd','b00000','b11'],
	'lw': ['imm-12','rs1', 'b010','rd','b00000','b11'],
	'lbu': ['imm-12','rs1', 'b100','rd','b00000','b11'],
	'lhu': ['imm-12','rs1', 'b101','rd','b00000','b11'],
	'sb': ['imm-7msb', 'rs2','rs1','b000','imm-5lsb','b01000','b11'],
	'sh': ['imm-7msb', 'rs2','rs1','b001','imm-5lsb','b01000','b11'],
	'sw': ['imm-7msb', 'rs2','rs1','b010','imm-5lsb','b01000','b11'],
	'jal': ['imm-12', 'b00000','b000','rd','b11011','b11'],
	'jalr': ['imm-12','rs1','b000','rd','b11001','b11'],
	'beq': ['imm-7msb', 'rs2','rs1'	,'b000','imm-5lsb', 'b11000', 'b11'],	
	'bne': ['imm-7msb', 'rs2','rs1','b001','imm-5lsb', 'b11000', 'b11'],	
	'blt': ['imm-7msb', 'rs2','rs1','b100','imm-5lsb', 'b11000', 'b11'],	
	'bge': ['imm-7msb', 'rs2','rs1','b101','imm-5lsb', 'b11000', 'b11'],	
	'bltu': ['imm-7msb', 'rs2','rs1','b110','imm-5lsb', 'b11000', 'b11'],	
	'bgeu': ['imm-7msb', 'rs2','rs1','b111','imm-5lsb', 'b11000', 'b11'],	
	'mul':	['b0000001','rs2','rs1','b000','rd','b01100', 'b11'],
	'mulh':	['b0000001','rs2','rs1','b001','rd','b01100', 'b11'],
	'mulhsu':	['b0000001','rs2','rs1','b010','rd','b01100', 'b11'],
	'mulhu': ['b0000001','rs2','rs1','b011','rd','b01100', 'b11'],
	'div': ['b0000001','rs2','rs1','b100','rd','b01100', 'b11'],
	'divu':	['b0000001','rs2','rs1','b101','rd','b01100', 'b11'],
	'rem': ['b0000001','rs2','rs1','b110','rd','b01100', 'b11'],
	'remu':	['b0000001','rs2','rs1','b111','rd','b01100', 'b11'],
}
