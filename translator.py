from instructions import instructionMappings, Data
import sys

def mapToInt(value):
	if (str(value[0]).lower() == 'r' or value[0]=='$'):
		return int(value[1:])
	return int(value)

def intToAddress(value):
	tempBin = hex(value)
	missingDigits = 8 - (len(tempBin) - 2)
	missingDigits = ''.join(['0' for i in range(missingDigits)])
	return missingDigits + tempBin[2:]

memoryAddr = ""
if len(sys.argv) > 1:
	printMemoryAddr = sys.argv[1][0] == 'T'
	if printMemoryAddr:
		currentAddress=int(sys.argv[1][1:], 16) 
else: 
	printMemoryAddr = False

if len(sys.argv) > 2:
	inputFilename = sys.argv[2]
else:
	inputFilename = 'input.txt'

if len(sys.argv) > 3:
	outputFilename = sys.argv[3]
else: 
	outputFilename = 'output.txt'


# writing to file
inputFile = open(inputFilename, 'r')
commandLines = inputFile.readlines()

# Using readlines()
outputFile = open(outputFilename, 'w')

# Strips the newline character
for command in commandLines:
	print('\n Nextline:', '"'+command.strip()+'"')
	instructions = command.split()
	currentCommandMapping = instructionMappings[str(instructions[0]).lower()]
	currentArg = 1
	translation = ''
	immStr = ''
	for arg in currentCommandMapping:
		if arg[0] == 'b':
			data = Data(arg, None)
		elif arg[-3:] == 'msb':
			data = Data(arg, mapToInt(instructions[currentArg]))
			immStr = data.binValue
			data.binValue = immStr[:7]
			currentArg += 1
		elif arg[-3:] == 'lsb':
			data.binValue = immStr[7:]
			data.size = 5
		else:
			data = Data(arg, mapToInt(instructions[currentArg]))
			currentArg += 1

		translation += data.binValue
		# DEBUG:
		# print(arg,'-',instructions, '-', currentArg, ':', data.binValue,'s.',data.size)
	if printMemoryAddr:
		memoryAddr = "0x"+intToAddress(currentAddress)+": "
		currentAddress += 1
	outputFile.write(memoryAddr+str(translation)+'\n')
	print("Translated to: "+translation)

outputFile.close()
